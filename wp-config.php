<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-2c' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3307' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l:0QC1`cZ[e^C$>jUvG8?F_O :LBI*WDx60-%>b$wVS0)@3R8nzEL;(v;D-D@M%X' );
define( 'SECURE_AUTH_KEY',  '=P.1Fm<UN)gq9Unr 9OLW3+s|TOgRa5sHE1?|sMt[tVx`N/vlJ,Tz(N~4%_Faf*N' );
define( 'LOGGED_IN_KEY',    'EsCXN;rlhfp}yY~9Y<e(e|z:a5s92(Q$me.W8o;ByBnj1x.?K)XWiHk19q8S78PJ' );
define( 'NONCE_KEY',        'sZ0?=/S5cz8xcL^5]]}9d4Ahq/prT(jDEZ G9gNhs4w/N?I3nd;[Q, igtE2]>o,' );
define( 'AUTH_SALT',        'dA^!=0p4[Xqw~<}`yqoD^@Wcui~4+<jr0c.UbzUADf)BG&`<w*~[aN x3[B%LfZ|' );
define( 'SECURE_AUTH_SALT', '>@`Eyp=DW,r(zQ0;@D2z/rY{Z|NQz@?m0D[s&s/l -heqM&Alb6)-BzH(m`[T=5t' );
define( 'LOGGED_IN_SALT',   ';ji{u;}{Mcx<<k/~ y%FX/] #66l:DUq4V.3!2xHZ{{Ms1$$h}&@@,2JHqjMKu0x' );
define( 'NONCE_SALT',       'Ai5+k2-/B:b.,qT9}^?_ft+(vB9Ob5y(fyE-]}Q>3X4/a|+NqW~UHI/JyoX),=6,' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
